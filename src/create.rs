use rand::thread_rng;
use rand::seq::SliceRandom;

pub fn create_maze(size: usize) -> Vec<Vec<i32>> {
    // create the grid (fill with 1s)
    let mut maze: Vec<Vec<i32>> = vec![vec![1; size * 2 + 1]; size * 2 + 1];

    // starting point
    maze[1][1] = 0;

    let mut directions = vec!([0, 1], [1, 0], [0, -1], [-1, 0]);
    let mut flag;

    // initialise stack
    let mut stack: Vec<[isize; 2]> = vec!([0, 0]);
    while stack.len() > 0 {
        flag = false;
        let [x, y] = stack[stack.len() - 1];

        directions.shuffle(&mut thread_rng());

        for [dx, dy] in directions.as_slice() {
            let nx = x + dx;
            let ny = y + dy;

            if nx >= 0 && ny >= 0 && nx < size as isize && ny < size as isize && maze[(2 * nx + 1) as usize][(2 * ny + 1) as usize] == 1 {
                maze[(2 * nx + 1) as usize][(2 * ny + 1) as usize] = 0;
                maze[(2 * x + dx + 1) as usize][(2 * y + dy + 1) as usize] = 0;
                stack.push([nx, ny]);
                flag = true;
                break;
            }
        }

        if !flag {
            stack.pop();
        }
    }

    // open tl and br
    maze[0][1] = 0;
    maze[size * 2][size * 2 - 1] = 0;
    return maze;
}

