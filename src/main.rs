mod solve;
mod create;
mod display;

use create::create_maze;
use solve::{find_solution_bfs, apply_solution};
use display::{display_maze, DisplayType};
use std::io;

fn main() {
    let mut out = io::stdout().lock();
    let mut maze;
    maze = create_maze(100);
    let path = find_solution_bfs(&maze);
    apply_solution(&mut maze, &path);
    display_maze(&mut out, &maze, &DisplayType::Emoji);
}
