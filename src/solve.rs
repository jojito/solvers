use std::collections::VecDeque;


pub fn apply_solution(maze: &mut Vec<Vec<i32>>, path: &Vec<[i32; 2]>) {
    for i in path.as_slice() {
        let [x, y] = *i;
        maze[y as usize][x as usize] = 2; 
    }
}

pub fn find_solution_bfs(maze: &Vec<Vec<i32>>) -> Vec<[i32; 2]> {
    let directions: Vec<[i32; 2]> = vec![[0, 1], [1, 0], [0, -1], [-1, 0]];
    // currently only works with end being bottom right
    let end = [( maze.len() - 2 ) as i32, ( maze.len() - 1 ) as i32];
    
    // list of visited nodes
    let mut visited = vec![vec![false; maze.len()]; maze.len()];
    visited[1][1] = true;

    // queue of nodes
    let mut queue = VecDeque::new();
    let start: Vec<[i32; 2]> = vec![];
    queue.push_back(([1, 1], start));
    while queue.len() > 0 {
        let (node, mut path) = queue.pop_front().unwrap();
        for [dx, dy] in directions.as_slice() {
            let next_node = [node[0] + dx, node[1] + dy];
            if next_node[0] < 0 || next_node[1] < 0 { /* bound checking */
                continue;
            }

            if next_node == end { /* found end */
                path.push(next_node);
                let mut i = vec!([1, 0], [1, 1]);
                i.append(&mut path);
                return i;
            }

            if maze[next_node[1] as usize][next_node[0] as usize] == 0 && !visited[next_node[1] as usize][next_node[0] as usize] {
                visited[next_node[1] as usize][next_node[0] as usize] = true;
                path.push(next_node);
                queue.push_back((next_node, path.clone()));
            }
        } 
    }

    return vec!([0i32, 0i32]);
}
