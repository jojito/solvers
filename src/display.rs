pub enum DisplayType {
    Raw,
    Emoji,
}

pub fn display_maze<T>(write_to: &mut T, maze: &Vec<Vec<i32>>, r#type: &DisplayType) where T: std::io::Write {
    for i in maze.iter() {
        for j in i {
            let char = match r#type {
                DisplayType::Raw => j.to_string(),
                DisplayType::Emoji => match j {
                            1 => "⬛".to_string(),
                            0 => "⬜".to_string(),
                            2 => "🟩".to_string(),
                            _ => "🟥".to_string(),
                }
            };

            match write!(write_to, "{}", char) {
                Ok(()) => (),
                Err(error) => {
                    println!("Error: {}", error);
                    return;
                },
            }
        }
        match write!(write_to, "\n") {
            Ok(()) => (),
            Err(error) => {
                println!("Error: {}", error);
                return;
            },
        }
    }
}

